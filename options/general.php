<?php
$general = new \Kyser\super_options();

$general->_addPage('imgSnap', 'imgSnap');
$general->_newGroup('general','imgSnap', 'imgSnap');
$general->_addField('general', 'css', 'Plugin Styles', 'textarea');
$general->_addField('general', 'snippet', 'jQuery API Snippet', 'textarea');
$general->_newGroup('images','imgSnap', 'imgSnap');
$general->_addField('images', 'cover1', 'Cover 1', 'image');
$general->_addField('images', 'cover2', 'Cover 2', 'image');
$general->_addField('images', 'cover3', 'Cover 3', 'image');
