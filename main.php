<?php
/**
 * Plugin Name: IMG Host
 * Plugin URI: https://www.kyser.io
 * Description: Allows a WP site to serve images across sites via an api
 * Version: 0.1
 * Author: Tommy Kyser
 * Author URI: https://www.kyser.io
 **/

function load_classes() // Classes :: Hook = init
{
	foreach(glob(ABSPATH . 'wp-content/plugins/img_host/' . "classes/*.php") as $class){
		require $class;
	}
}
add_action('init', 'load_classes');

function load_tasks() // Tasks :: Hook = wp_loaded
{
	foreach(glob(ABSPATH . 'wp-content/plugins/img_host/' . "tasks/*.php") as $task){
		require $task;
	}
}
add_action('wp_loaded', 'load_tasks');

function load_shortcodes() // Shortcodes :: Hook = wp_head
{
	foreach(glob(ABSPATH . 'wp-content/plugins/img_host/' . "shortcodes/*.php") as $shortcode){
		require $shortcode;
	}
}
add_action('wp_head', 'load_shortcodes');



function load_options() // Option Pages :: Hook = init

{
	foreach(glob(ABSPATH . 'wp-content/plugins/img_host/' . "options/*.php") as $option){
		require $option;
	}
}
add_action('init', 'load_options');

