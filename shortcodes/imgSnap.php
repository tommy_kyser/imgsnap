<?php
// Add Shortcode
function imgSnap( $atts ) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'form_title' => '',
		),
		$atts
	);
	?>
	<style>
		<?php the_field('css', 'options'); ?>
	</style>
	<div class="imgSnap">
		<h1><?php echo $atts['form_title'] ?></h1>
		<?php acf_form_head(); ?>
		<?php acf_form( 'imgSnapForm' ); ?>
		<div class="img_field">
			<h3>API Snippet to Place on Remote Site:</h3>
			<pre>
<code>
jQuery(document).ready(function ($) {
	$.ajax({
		url: "<?php echo get_site_url(); ?>/?imgSnap=true", success: function (result) {
		const images = JSON.parse(result); //array of images, (cover1, cover2, cover3)
		console.log(images);
		<?php the_field( 'snippet', 'options' ); ?>
		}
	});
}); // jQuery end
</code>
</pre>
		</div>
	</div>
	<?php
}

add_shortcode( 'imgSnap', 'imgSnap' );

