**LIVE DEMO: https://imghost.kyser.io/
WP login: admin/Scooby25**


**PREREQUISITES:**
ACF PRO

**INSTALL:**

Either clone/pull this repo or download the repo as a zip file.

Goto plugins in WP admin, and add new.
Upload the zip file inside this repo and install/activate. (imghost-plugin.zip)

**WORDPRESS HOST CONFIG AND USAGE:**

place this shortcode anywhere you want the front end form to appear:

```
[imgSnap form_title="Cover Images"]
```

In WP Admin you will see a new options page called imgSnap
From here you will be able to add styles to the form and also edit the jQuery snippet.
I have also included the fields for the images on the backend in this options page

**CUSTOMIZATION:**

In WP Admin goto imgSnap, you will see a styles field. 
This field can contain any css you would like to style the front end form on the wordpress host.

The jQuery Snippet field by default contains a barebones functional representation of the working js-api.

You can re-write this in anyway you so desire, the limitation being that whatever you write will be nested within the jQuery AJAX call. 
This was intentional for simplicity.


Front End Where the Form is placed:

the user will be able to upload the cover images and change them

As long as there are images present the API will return a JSON object containing the URLs to the images.

The Snippet is displayed on the front end for ease of use, so that the user responsible for displaying images on another site can simply copy and paste the snippet.


**API:**


On the WP host this ```?imgSnap=true``` is the query string that will return the JSON object 

This is what will be returned:
```JSON
{ 
    "cover1": "https://hostURL.tld/wp-content/uploads/YEAR/MONTH/image.file",
    "cover2": "https://hostURL.tld/wp-content/uploads/YEAR/MONTH/image.file",
    "cover3": "https://hostURL.tld/wp-content/uploads/YEAR/MONTH/image.file"
}
```


The snippet displayed on the front end will always use the hosts wp site url.


**API EXAMPLE USAGE:**
This is a working example that can be placed on any site for testing purposes.

```javascript

			jQuery(document).ready(function ($) {
				$.ajax({
					url: "https://imghost.kyser.io/?imgSnap=true", success: function (result) {
						const images = JSON.parse(result); //array of images, (cover1, cover2, cover3)
						console.log(images);
						var container = '<div class="imgs"></div>';
						$('body').append(container);
						$('.imgs').text(images.cover1);
						var styles = `<style></style>`;
                        $('.imgs').parent().append(styles);	
					}
				});
			}); // jQuery end

```

On the target site where you will display the images, simply control and style the markup within the jQuery ajax snippet.

providing styles for the images on the remote site can be as simple as writing css in the raw template literal form or you may use the power of javascript to do it in another fashion.

example:
```javascript
    var styles = `
        <style>
            .cover-imgs {
                max-width:50vh;
                margin:auto;
            }
        </style>
    `;
       $('body').prepend(styles);	
```
